class WinCalculator {
    constructor(cards){
        this.cards = cards;
        this.suits = this.cards.map(card => card.suit);
         this.ranks = this.cards.map(card => card.rank);

        this.isFlush = this.suits.every(suit => suit === this.suits[0]);

    }

    isRoyalFlush() {
        return this.isFlush &&
        this.ranks.includes('10') &&
        this.ranks.includes('J') &&
        this.ranks.includes('Q') &&
        this.ranks.includes('K') &&
        this.ranks.includes('A')
    }

    isPair(){

    }
    getBestHand(){
       if(this.isRoyalFlush()) {
           return 'Royal Flush';
       } else if(this.isFlush) {
           return 'Flush';
       } else if (this.isPair()){
           return 'A Pair';

       } else {
           return 'Nothing'
       }
    }


}
export default WinCalculator;